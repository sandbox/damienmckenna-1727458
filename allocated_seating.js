//$Id$
/*
 * @file allocated_seating.js
 * Provides js to get designer and selector to work
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

Drupal.behaviors.allocated_seating_design = function(context) {
  function syncSeats(tab) {
    var values = [], processed = [];
    var seats = tab.find('li');
    var inputs = tab.find('input[type=hidden]');
    seats.each(function() {
      var s = $(this).text();
      $(inputs[seats.index($(this))]).val(s);
    })
  }
  $(".allocated-seating-seats:not(.no-drag)").sortable({
    items: 'li',
    update: function(event, ui) {
      syncSeats($(this));
    }
  }).disableSelection();

  //this largely ripped off from http://jqueryui.com/demos/sortable/#connect-lists-through-tabs
  var $tabs = $("#allocated-seating-tabs").tabs();

  var $tab_items = $("ul:first li",$tabs).droppable({
    accept: ".connectedSortable li",
    hoverClass: "ui-state-hover",
    drop: function(ev, ui) {
      var $item = $(this);
      var $list = $($item.find('a').attr('href')).find('.connectedSortable');
      
      //get the tab we removed it from
      var $old_tab = ui.draggable.parent();
      var seats = $list.find('li');
      //the last seat pops off the end
      var seat = seats.length -1;
      //get the swap seat on this tab
      var $swap = seats[seat];
      
      ui.draggable.addClass('moving').hide('slow', function() {
        $tabs.tabs('select', $tab_items.index($item));
        $(this).prependTo($list).show('slow', function() {
          $(this).removeClass('moving');
          //move the first one to the old tab
          $($swap).addClass('moving').hide('slow', function() {
            $tabs.tabs('select', $('.ui-tabs-panel').index($old_tab.parent()));
            $(this).appendTo($old_tab).show('slow', function() {
              $(this).removeClass('moving');
              var handle = [$list, $old_tab];
              $.each(handle, function(ix, v) {
                syncSeats($(v));
              });
            });
          });
        });
      });
      
      
      
    }
  });

}

Drupal.behaviors.allocated_seating_select = function(context) {
  
}